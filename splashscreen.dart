import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  SplashScreenState createState() {
    return new SplashScreenState();
  }
}
  
const TextStyle textStyle = TextStyle(
  color: Colors.white,
  fontFamily: 'OpenSans', 
)
class SplashScreenState extends State<SplashScreen> with SingleTickerProviderStateMixin{

  AnimationController controller;
  animation<double> animation;

  @override 
  void iniState(){
    super.initState();
    controller = AnimationController(
      duration: (milliseconds: 2000),
      vsync: this,
    );

    animation = Tween(begin: 0.0, end: 1.0). animate(controller)
      ..addListener(() {
        setState(() {});
    });

    controller.forward();
  }

  @override
  Void dispose() {
    super.dispose();
    controller.dispose();
  } 
  final background = Container(
    decoration: BoxDecoration(
      image: DecorationImage(
        image: AssetImage('assets/image/background.jpg'),
        fit: BoxFit.cover,
      ),
    ),
  );

  final greenOpacity = Container(
    color: Color(0xAA69F0CF),
  );

  

  final description = new FadeTransition(
    opacity: animation,
    child: new slideTransition(
      position: Tween<Offset>(begin: Offset(0.0,-0.8), end: Offset,
          .animate(controller),
      child: Text(
      'Perfect Elegant local handmade inners.',
      textAlign: TextAlign.center,
      style: textStyle.copyWith(fontSize: 23.0),
      )
    ),
  ));

 Widget button(String label, Function onTap) {
   return Material(
    color: Color(0xBB00D699),
    borderRadius: BorderRadius.circular(30.0),
    child: InkWell(
      onTap: onTap,
      splashColor: Colors.white24,
      highlightColor: Colors.white10,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 13.0),
        child: Center(
          child: Text(label,style: textStyle.copyWith(fontSize: 18.0),),
        ),
      ),
    ),
  );
  }

  final separator = Row(
    mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget> [
      Container(width: 18.0, height: 3.0, color: Colors.white),
      Container(width: 18.0, height: 3.0, color: Colors.white),
    ],
  );
  
  final logo = new ScaleTransition(
    scale: animation,
      child: Image.asset(
      'assets/images/logo.png',
      width: 180.0,
      height: 180.0,
    ),
    );

    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          background,
          greenOpacity,
          new SafeArea(
            child: new Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  logo,
                  SizedBox(height: 30.0),
                  description,
                  SizedBox(height: 30.0),
                  button('log in', () {print('dashboard');}),
                  SizedBox(height: 30.0),
                  button('create an account', () {print('profile');}),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}